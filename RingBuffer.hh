// This file is part of RingBuffer.

// RingBuffer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// RingBuffer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with RingBuffer.  If not, see <http://www.gnu.org/licenses/>.
				     
#include <array>
#include <numeric>

template<typename T, size_t N>
class RingBuffer : public std::array<T, N>{
protected:
  size_t first;
  bool full;
public:
  RingBuffer();
  void push(T x);
  constexpr T at(std::size_t i);
  constexpr T operator[](std::size_t i);
  template<typename vect> T dot(const vect& u);
  constexpr T squaredNorm();
  bool isFull();
};

template<typename T, size_t N>
RingBuffer<T, N>::RingBuffer() : std::array<T, N>{0}, first(0), full(false){
}

template<typename T, size_t N>
void RingBuffer<T, N>::push(T x){
  std::array<T, N>::operator[](first) = x;
  first = (first + 1)%N;
  if(first == 0 && !full)
    full = true;
}

template<typename T, size_t N>
constexpr T RingBuffer<T, N>::at(std::size_t i){
  return std::array<T, N>::at((first + i)%N);
}

template<typename T, size_t N>
constexpr T RingBuffer<T, N>::operator[](std::size_t i){
  return std::array<T, N>::operator[]((first + i)%N);
}

template<typename T, size_t N>
template<typename vect>
T RingBuffer<T, N>::dot(const vect& u){
  T prod = 0;

  for(int i = 0; i < N - first - 1; i++)
    prod += std::array<T, N>::operator[](first + i)*u[i];

  for(int i = N - first; i < N; i++)
    prod += std::array<T, N>::operator[](first + i - N)*u[i];
  
  return prod;
}

template<typename T, size_t N>
constexpr T RingBuffer<T, N>::squaredNorm(){
  return std::inner_product(std::array<T, N>::begin(), std::array<T, N>::end(), std::array<T, N>::begin(), 0.0);
}

template<typename T, size_t N>
bool RingBuffer<T, N>::isFull(){
  return full;
}
